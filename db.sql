 /* _        _     _ */
/* | |_ __ _| |__ | | ___  ___ */
/* | __/ _` | '_ \| |/ _ \/ __| */
/* | || (_| | |_) | |  __/\__ \ */
 /* \__\__,_|_.__/|_|\___||___/ */

create extension pgcrypto;
select * from pg_proc where proname like 'gen_random_%';

create table email_table_ (
	id UUID primary key default gen_random_uuid(),
	email text not null unique,
    firstName text not null,
    lastName text not null,
    address text not null,
    dateOfBirth date not null
);

