package dev.team2.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

	/**
	 * TARGET environment variable can be used to select alternate database connection configurations.
	 * Connects to AWS RDS by default or if unset.
	 */
	private static final String TARGET = System.getenv("TARGET");

	private static Connection connection;

	public static Connection getConnection() throws SQLException {
		if (connection == null || connection.isClosed()) {
			String url;
			String USERNAME;
			String PASSWORD;

			if (TARGET == null) {
				url = "jdbc:postgresql://trainingdb.c4dnn2jbi3sh.us-east-2.rds.amazonaws.com:5432/postgres";
				USERNAME = System.getenv("USERNAME");
				PASSWORD = System.getenv("PASSWORD");

			} else {
				switch (TARGET) {
					case "local":
						url = "jdbc:postgresql://localhost:5432/p1db";
						USERNAME = System.getenv("USERNAME_LOCAL");
						PASSWORD = System.getenv("PASSWORD_LOCAL");
						break;

					default:
						url = "jdbc:postgresql://trainingdb.c4dnn2jbi3sh.us-east-2.rds.amazonaws.com:5432/postgres";
						USERNAME = System.getenv("USERNAME");
						PASSWORD = System.getenv("PASSWORD");
						break;
				}
			}
			connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
		}
		return connection;
	}
}
